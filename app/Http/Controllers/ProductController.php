<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Get products in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function products(Request $request)
    {
        try
        {
            // get products
            $products = Product::all();

            // response
            return $this->respondWithSuccess(['products' => $products]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'title' => 'required',
                    'description' => 'required',
                    'image' => 'required',
                    'meta_data' => 'required',
                    'category_id' => 'required',
                    'subcategory_id' => 'required',
                    'price' => 'required',
                    'quantity' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // response
                return $this->respondWithError($error);
            }

            // extract all
            extract($request->all());

            // start transaction
            \DB::beginTransaction();

                // save product
                $product = new Product;
                $product->title = $title;
                $product->description = $description;

                // capture image
                if($request->hasFile('image'))
                {
                    $path = $request->image->getRealPath();
                    $image = file_get_contents($path);
                    $base64 = base64_encode($image);
                    $product->image = $base64;
                }

                $product->meta_data = $meta_data;
                $product->category_id = $category_id;
                $product->subcategory_id = $subcategory_id;
                $product->price = $price;
                $product->quantity = $quantity;
                $product->save();

            // commit transaction
            \DB::commit();

            // response
            return $this->respondWithSuccess(['message' => 'Product is successfull added!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            // get product
            $product = Product::findOrFail($id);

            // response
            return $this->respondWithSuccess(['product' => $product]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'title' => 'required',
                    'description' => 'required',
                    'image' => 'required',
                    'meta_data' => 'required',
                    'category_id' => 'required',
                    'subcategory_id' => 'required',
                    'price' => 'required',
                    'quantity' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // response
                return $this->respondWithError($error);
            }

            // start transaction
            \DB::beginTransaction();

                // update product
                $product = Product::findOrFail($id);
                $product->title = $title ?? NULL;
                $product->description = $description ?? NULL;

                // capture image
                if($request->hasFile('image'))
                {
                    $path = $request->image->getRealPath();
                    $image = file_get_contents($path);
                    $base64 = base64_encode($image);
                    $product->image = $base64;
                }
                else{ $product->image = $image ?? NULL; }

                $product->meta_data = $meta_data ?? NULL;
                $product->category_id = $category_id ?? NULL;
                $product->subcategory_id = $subcategory_id ?? NULL;
                $product->price = $price ?? NULL;
                $product->quantity = $quantity ?? NULL;
                $product->save();

            // commit transaction
            \DB::commit();

            // response
            return $this->respondWithSuccess(['message' => 'Product is successfull updated!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            // delete product
            Product::findOrFail($id)->delete();

            // response
            return $this->respondWithSuccess(['message' => 'Product is successfull deleted!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }
}
