<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Get orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orders(Request $request)
    {
        try
        {
            // get orders
            $orders = Orders::all();

            // response
            return $this->respondWithSuccess(['orders' => $orders]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'category_id' => 'required',
                    'subcategory_id' => 'required',
                    'product_id' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // response
                return $this->respondWithError($error);
            }

            // extract all
            extract($request->all());

            // start transaction
            \DB::beginTransaction();

                // save order
                $order = new Order;
                $order->category_id = $category_id;
                $order->subcategory_id = $subcategory_id;
                $order->product_id = $product_id;
                $order->qty_to_purchase = $qty_to_purchase ?? NULL;
                $order->time_of_purchase = $time_of_purchase ?? NULL;
                $order->save();

            // commit transaction
            \DB::commit();

            // response
            return $this->respondWithSuccess(['message' => 'Order is successfull added!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            // get order
            $order = Order::with('category')
            ->with('subcategory')
            ->with('product')
            ->findOrFail($id);

            // response
            return $this->respondWithSuccess(['order' => $order]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'category_id' => 'required',
                    'subcategory_id' => 'required',
                    'product_id' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // response
                return $this->respondWithError($error);
            }

            // extract all
            extract($request->all());

            // start transaction
            \DB::beginTransaction();

                // save order
                $order = new Order;
                $order->category_id = $category_id ?? NULL;
                $order->subcategory_id = $subcategory_id ?? NULL;
                $order->product_id = $product_id ?? NULL;
                $order->qty_to_purchase = $qty_to_purchase ?? NULL;
                $order->time_of_purchase = $time_of_purchase ?? NULL;
                $order->save();

            // commit transaction
            \DB::commit();

            // response
            return $this->respondWithSuccess(['message' => 'Order is successfull updated!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            // delete order
            Order::findOrFail($id)->delete();

            // response
            return $this->respondWithSuccess(['message' => 'Order is successfull deleted!']);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }
}
