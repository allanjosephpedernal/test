<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable()->default(NULL);
            $table->integer('subcategory_id')->nullable()->default(NULL);
            $table->integer('product_id')->nullable()->default(NULL);
            $table->integer('qty_to_purchase')->nullable()->default(NULL);
            $table->datetime('time_of_purchase')->nullable()->default(NULL);
            $table->timestamps();

            $table->index('category_id');
            $table->index('subcategory_id');
            $table->index('product_id');
            $table->index('qty_to_purchase');
            $table->index('time_of_purchase');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
