<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function subcategory()
    {
        return $this->hasOne(Subcategory::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
