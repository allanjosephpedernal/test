<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function subcategory()
    {
        return $this->hasOne(Subcategory::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
