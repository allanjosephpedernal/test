<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Models\Subcategory;

class SubcategoryController extends Controller
{
    /**
     * Get subcategories in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Int $category_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function subcategories(Request $request, $category_id)
    {
        try
        {
            // get subcategories
            $subcategories = Subcategory::where('category_id',$category_id)->get();

            // response
            return $this->respondWithSuccess(['subcategories' => $subcategories]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }
}
