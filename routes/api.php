<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'], function () {

    Route::get('/categories','CategoryController@categories');
    Route::get('/subcategories/{category_id}','SubcategoryController@subcategories');

    Route::get('/products','ProductController@products');
    Route::resource('/product','ProductController',['only'=>['store','show','update','destroy']]);

    Route::get('/orders','OrderController@orders');
    Route::resource('/order','OrderController',['only'=>['store','show','update','destroy']]);

});