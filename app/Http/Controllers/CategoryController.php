<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Get category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories(Request $request)
    {
        try
        {
            // get categories
            $categories = Categories::all();

            // response
            return $this->respondWithSuccess(['categories' => $categories]);
        }
        catch(\Exception $e)
        {
            // response
            return $this->respondWithError(['message' => $e->getMessage()],500);
        }
    }
}
